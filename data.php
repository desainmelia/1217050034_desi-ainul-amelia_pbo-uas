<?php
    require_once('conn.php');

    $query = mysqli_query($connection, "SELECT * FROM users");

    $result = array();
    while($row = mysqli_fetch_array($query)){
        array_push($result, array(
            'username' => $row['username'],
            'password' => $row['password'],
            'package' => $row['package'],
            'payment' => $row['payment']
        ));
    }

    echo json_encode(
        array('result' => $result)
    );
?>