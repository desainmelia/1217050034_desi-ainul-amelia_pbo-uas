### 1217050034_Desi Ainul Amelia_PBO UAS

#### Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
- Use case user
- Use case manajemen perusahaan
- Use case direksi perusahaan (dashboard, monitoring, analisis)
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__356_.png)
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__357_.png)
#### Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
- Class Diagram User
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/PBO-USER.drawio.png)

- Class Diagram Direksi dan manajemen
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/PBO-Direksi_Manajemen.drawio.png)

#### Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/login_process.php
   
#### Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/rating_process.php

#### Mampu menunjukkan dan menjelaskan konektivitas ke database
https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/register_process.php

#### Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
1. https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/conn.php
Melakukan Konfigurasi Ke -SQL untuk bisa akses data pengguna
2. https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/create.php
Kedua untuk menambahkan data didalam webservice kita juga membutuhkan file dengan query Insert yang dimana jika dijalankan maka seperti gambar dibawah ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__360_.png)
3. https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/data.php
Ketiga adalah proses get data, atau read. dimana data yang sudah ditambahkan akan tampil seperti dibawah ini:
![Dokumetasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__361_.png)
4. https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/update.php
Keempat adalah proses update, dimana membutuhkan query update didalam filenya dengan proses dibawah ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__362_.png)
jika di get akan muncul seperti ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__363_.png)
password sesuai query terganti pada username araatiara
5. https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/blob/main/delete.php
terakhir ada delete yaitu untuk menghapus data, dan juga menggunakan query delete akan muncul prosesnya seperti ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__364_.png)
jika di get akan muncul seperti ini:
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__365_.png)

dalam proses kemunculan webservicenya saya menggunakan postman.


#### Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
1. Login 
    - Header: Header pada tampilan utama login adalah judul Aplikasi yaitu Netflix
    - Body/Form: Terdapat Menu Sign in yang dimana terdapat space untuk mengisi username dan password yang telah terdaftar, dan juga registrasi untuk yang belum memiliki akun.
    - Tombol dan Tautan: GUI juga menyediakan tombol dan tautan interaktif yang memungkinkan pengguna untuk melakukan tindakan tertentu, seperti mengklik tombol "login" pada menu login untuk  mengikuti tautan menuju halaman play film.
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__366_.png)

2. Registrasi
    - Header: Header pada tampilan registrasi adalah judul Registrasi yaitu Registrasi Form
    - Body/Form: Jika belum terdaftar bisa melakukan registrasi, dimana yang berisikan untuk mengisi data calon user.
    - Tombol dan Tautan: GUI juga menyediakan tombol dan tautan interaktif yang memungkinkan pengguna untuk melakukan tindakan tertentu, seperti mengklik tombol "Registrasi" pada menu login dan registrasi untuk  mengikuti tautan menuju halaman play film.
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__367_.png)

3. Play Film
    - Header: Header pada tampilan playfilm adalah judul Aplikasi yaitu Netflix.
    - Body: Terdapat salam pembuka untuk user, dan juga daftar film yang tersedia.
    - Tombol dan Tautan: GUI juga menyediakan tombol dan tautan interaktif yang memungkinkan pengguna untuk melakukan tindakan tertentu, seperti mengklik tombol "One Piece" pada menu playfilm dan untuk  mengikuti tautan menuju halaman pemutaran film.
![Dokumentasi](https://gitlab.com/desainmelia/1217050034_desi-ainul-amelia_pbo-uas/-/raw/main/Dokumentasi/Screenshot__368_.png)

#### Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
- URL Input:Memasukkan URL tujuan ke dalam bidang input ini. URL ini mewakili alamat tujuan untuk melakukan koneksi HTTP,"http://localhost/basic-slim/index.php".

- HTTP Methods: Terdapat pilihan metode HTTP yang tersedia, seperti GET, POST.

- Headers: Pengguna dapat menambahkan header tambahan yang akan dikirimkan dalam koneksi HTTP. Header ini berisi informasi tambahan yang dapat digunakan oleh server untuk memproses permintaan dengan benar.

#### Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/392i-HKkNLU


# BONUS !!! Mendemonstrasikan penggunaan Machine Learning
