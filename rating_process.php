<?php

//observer design pattern

interface RatingObserverInterface {
    public function update($username, $rating);
}

class RatingSubject {
    private $observers = array();

    public function attach(RatingObserverInterface $observer) {
        $this->observers[] = $observer;
    }

    public function detach(RatingObserverInterface $observer) {
        $key = array_search($observer, $this->observers);
        if ($key !== false) {
            unset($this->observers[$key]);
        }
    }

    public function notify($username, $rating) {
        foreach ($this->observers as $observer) {
            $observer->update($username, $rating);
        }
    }
}

class RatingLogger implements RatingObserverInterface {
    public function update($username, $rating) {
        // Simpan rating ke log
        $logMessage = "User: $username, Rating: $rating";
        file_put_contents('rating.log', $logMessage . PHP_EOL, FILE_APPEND);
    }
}

class RatingStatistic implements RatingObserverInterface {
    public function update($username, $rating) {
        // Perbarui statistik rating film
        // ...
    }
}

// Koneksi ke database
$conn = new mysqli("localhost", "root", "", "netflix");

// Periksa koneksi
if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}

$rating = $_POST['rating'];
$username = $_SESSION['username'];

$sql = "INSERT INTO ratings (username, rating) VALUES ('$username', '$rating')";
$result = $conn->query($sql);

// Subject yang akan diobservasi
$ratingSubject = new RatingSubject();

// Attach observer yang ingin dijalankan
$ratingSubject->attach(new RatingLogger());
$ratingSubject->attach(new RatingStatistic());

// Melakukan notifikasi ke observer
$ratingSubject->notify($username, $rating);

header("Location: index.php");

?>

//terdapat tiga kelas yang terlibat:

//RatingSubject: Kelas ini bertindak sebagai subjek yang diamati. Subjek memiliki metode attach() untuk menambahkan observer, detach() untuk menghapus observer, dan notify() untuk memberitahu setiap observer tentang perubahan yang terjadi. Dalam implementasi ini, subjek adalah objek yang menerima rating dari pengguna.

//RatingLogger dan RatingStatistic: Kedua kelas ini merupakan pengamat yang mengimplementasikan antarmuka RatingObserverInterface. Setiap pengamat memiliki metode update() yang dipanggil oleh subjek ketika terjadi perubahan. RatingLogger mencatat rating ke dalam file log, sedangkan RatingStatistic mengupdate statistik rating film.

//attach(), detach(), dan notify(): Metode-metode ini digunakan oleh subjek untuk mengelola daftar observer dan memberitahukan perubahan kepada observer. Dalam contoh ini, observer (RatingLogger dan RatingStatistic) ditambahkan ke subjek menggunakan metode attach() dan diberitahu tentang perubahan rating menggunakan metode notify().

//Penggunaan pola desain Observer dalam contoh ini memungkinkan aplikasi untuk memiliki fleksibilitas dan skalabilitas yang lebih baik. Jika ada lebih banyak tugas yang perlu dilakukan ketika rating berubah di masa depan, kita dapat dengan mudah menambahkan pengamat baru tanpa mengubah subjek atau pengamat yang sudah ada. Selain itu, penggunaan pola ini juga memisahkan logika subjek dan observer, sehingga memungkinkan perubahan pada salah satu bagian tanpa memengaruhi yang lainnya.