<?php
require_once('conn.php');
parse_str(file_get_contents('php://input'), $value);

$username = $value['username'];

$query = "DELETE FROM users WHERE username = ?";
$stmt = mysqli_prepare($connection, $query);
mysqli_stmt_bind_param($stmt, "s", $username);
$result = mysqli_stmt_execute($stmt);

if ($result) {
    echo json_encode(array('message' => 'Deleted'));
} else {
    echo json_encode(array('message' => 'Error'));
}
?>
