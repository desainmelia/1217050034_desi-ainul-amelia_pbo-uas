<?php
require_once('conn.php');
parse_str(file_get_contents('php://input'), $value);

$username = $value['username'];
$password = $value['password'];

$query = "UPDATE users SET password = ? WHERE username = ?";
$stmt = mysqli_prepare($connection, $query);
mysqli_stmt_bind_param($stmt, "ss", $password, $username);
$result = mysqli_stmt_execute($stmt);

if ($result) {
    echo json_encode(array('message' => 'Updated'));
} else {
    echo json_encode(array('message' => 'Error'));
}
?>
