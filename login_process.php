<?php

// Abstraksi: Menggunakan interface untuk abstraksi UserRepositoryInterface
interface UserRepositoryInterface //Single Responsibility Principle (SRP), Interface Segregation Principle (ISP)
{
    public function findByUsernameAndPassword($username, $password);
}

// Enkapsulasi: DatabaseConnection hanya digunakan di dalam UserRepository, sehingga dideklarasikan sebagai private property di UserRepository
class UserRepository implements UserRepositoryInterface // Liskov Substitution Principle (LSP)
{
    private $connection;

    public function __construct(DatabaseConnection $connection) //Dependency Inversion Principle (DIP)
    {
        $this->connection = $connection->getConnection();
    }

    public function findByUsernameAndPassword($username, $password)
    {
        $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
        $result = $this->connection->query($sql);

        return $result->num_rows > 0;
    }
}

// Enkapsulasi: AuthenticationService menggunakan UserRepository melalui UserRepositoryInterface
class AuthenticationService //Single Responsibility Principle (SRP)
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function authenticate($username, $password)
    {
        if ($this->userRepository->findByUsernameAndPassword($username, $password)) {
            $_SESSION['username'] = $username;
            header("Location: index.php");
        } else {
            echo "Invalid username or password.";
        }
    }
}

// Pewarisan (Inheritance): Membuat class DatabaseConnection sebagai parent class untuk DatabaseConnectionMySQL
class DatabaseConnection //Single Responsibility Principle (SRP)
{
    private $conn;

    public function __construct($host, $username, $password, $database)
    {
        $this->conn = new mysqli($host, $username, $password, $database);
        if ($this->conn->connect_error) {
            die("Koneksi gagal: " . $this->conn->connect_error);
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }
}

// Polimorfisme: Membuat class DatabaseConnectionMySQL yang merupakan turunan dari DatabaseConnection
class DatabaseConnectionMySQL extends DatabaseConnection  //Liskov Substitution Principle (LSP)
{
    // Dapat menambahkan method khusus untuk koneksi database MySQL jika diperlukan
}
// Penggunaan

session_start();

$databaseConnection = new DatabaseConnectionMySQL("localhost", "root", "", "netflix");
$userRepository = new UserRepository($databaseConnection);
$authenticationService = new AuthenticationService($userRepository);

$username = $_POST['username'];
$password = $_POST['password'];

$authenticationService->authenticate($username, $password);


//Dalam program tersebut, berikut adalah deteksi prinsip SOLID yang terdapat dalam kode:

//1. Single Responsibility Principle (SRP):
//- Kelas `DatabaseConnection` bertanggung jawab untuk membuat koneksi dengan database.
//- Kelas `UserRepository` bertanggung jawab untuk mengakses data pengguna.
//- Kelas `AuthenticationService` bertanggung jawab untuk melakukan proses otentikasi.

//2. Open/Closed Principle (OCP):
//- Kelas-kelas yang ada dapat diperluas dengan menambahkan metode baru atau kelas turunan tanpa mengubah kode yang sudah ada.

//3. Liskov Substitution Principle (LSP):
//- Kelas `UserRepository` mengimplementasikan `UserRepositoryInterface` dan dapat digunakan secara interchangeably dengan interface tersebut.

//4. Interface Segregation Principle (ISP):
//- `UserRepositoryInterface` menyediakan satu metode yang diperlukan untuk operasi pencarian pengguna.

//5. Dependency Inversion Principle (DIP):
//- Kelas `UserRepository` menerima `DatabaseConnection` melalui konstruktor, sehingga dependensi dibalik dan kelas `UserRepository` bergantung pada abstraksi `DatabaseConnection`.

//Penerapan prinsip-prinsip SOLID ini membantu untuk membuat kode menjadi lebih modular, terpisah, dan mudah diperluas dengan mengikuti prinsip-prinsip desain yang baik.
