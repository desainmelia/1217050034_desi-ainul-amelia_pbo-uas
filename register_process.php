<?php
session_start();
// Koneksi ke database
$conn = new mysqli("localhost", "root", "", "netflix");

// Periksa koneksi
if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $package = $_POST['package'];
    $payment = $_POST['payment'];

    // Lakukan validasi dan verifikasi lainnya

    // Lakukan sanitasi pada input
    $username = mysqli_real_escape_string($conn, $username);
    $password = mysqli_real_escape_string($conn, $password);
    $package = mysqli_real_escape_string($conn, $package);
    $payment = mysqli_real_escape_string($conn, $payment);

    // Query untuk memasukkan data ke dalam database
    $sql = "INSERT INTO users (username, password, package, payment) VALUES ('$username', '$password', '$package', '$payment')";

    if ($conn->query($sql) === TRUE) {
        $_SESSION['username'] = $username;
        header("Location: index.php");
        exit;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>
